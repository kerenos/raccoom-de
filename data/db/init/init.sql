-- Adminer 4.7.7 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

CREATE DATABASE IF NOT EXISTS `raccoom`;
USE `raccoom`;

DROP TABLE IF EXISTS `nutzer`;
CREATE TABLE `nutzer` (
  `iban` char(22) NOT NULL,
  `nachname` char(20) DEFAULT NULL,
  `vorname` char(20) DEFAULT NULL,
  `gebdat` date DEFAULT NULL,
  `strasse` char(30) DEFAULT NULL,
  `plz` char(5) DEFAULT NULL,
  `ort` char(30) DEFAULT NULL,
  PRIMARY KEY (`iban`)
);

INSERT INTO `nutzer` (`iban`, `nachname`, `vorname`, `gebdat`, `strasse`, `plz`, `ort`) VALUES
('1012135179361459802423',	'Schulz',	'Volkfried',	'1999-07-17',	'Deisterstrasse 17',	'31785',	'Hameln'),
('7857198835141134341229',	'Ahlswede',	'Gottfried',	'1999-07-17',	'Grupenhagener Strasse 46',	'31855',	'Aerzen'),
('7632116895728101596287',	'Schmidt',	'Gottlieb',	'1999-07-17',	'Susannenstrasse 36',	'20357',	'Hamburg'),
('5329030121139095024565',	'Ruckzuck',	'Erna',	'1990-07-12',	'Rosenhofstrasse 7',	'20357',	'Hamburg');

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `prodId` int(11) NOT NULL AUTO_INCREMENT,
  `prodName` varchar(100) NOT NULL,
  `prodDesc` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`prodId`)
);

INSERT INTO `products` (`prodId`, `prodName`, `prodDesc`) VALUES
(1,	'Erdnuss',	'Die Erdnuss ist eine Pflanzenart in der Unterfamilie der Schmetterlingsblütler innerhalb der Familie der Hülsenfrüchtler.'),
(2,	'Cashewnuss',	'Die Cashewnuss gehört zu der  Familie der Sumachgewächse gehöriger Baum. Er wächst in tropischem Klima und trägt Cashewäpfel und Cashewkerne.'),
(3,	'Haselnuss',	'Die Haselnuss ist eine Nuss.'),
(4,	'Macadamia',	'Macadamia ist eine Pflanzengattung in der Familie der Silberbaumgewächse. Sie ist vor allem durch die Frucht der beiden Arten Macadamia integrifolia und Macadamia tetraphylla bekannt, deren Samen Macadamianuss genannt wird.'),
(5,	'Mandel',	'Der Mandelbaum ist eine Pflanzenart der Gattung Prunus in der Familie der Rosengewächse.'),
(6,	'Kastanien',	'Die Kastanien oder Edelkastanien sind eine Pflanzengattung in der Familie der Buchengewächse. Die Gattung ist mit etwa zwölf Baum- und Straucharten in der nördlich gemäßigten Zone verbreitet.'),
(7,	'Paranuss',	'Der Paranussbaum gehört zu den Topffruchtbaumgewächsen. Den botanischen Gattungsnamen Bertholletia erhielt der Baum zu Ehren des französischen Chemikers Claude Louis Berthollet.'),
(8,	'Pistazie',	'Der Pistazienbaum oder vereinfacht Pistazie ist eine Pflanzenart innerhalb der Familie der Sumachgewächse. Zur Unterscheidung von den anderen Arten der Gattung Pistazien wird sie genauer Echte Pistazie genannt und ihre Steinfrucht Pistazie.'),
(9,	'Walnuss',	'Die Walnuss ist eine Nuss und macht sich besonders schön auf der Stirn von Sandro');

-- 2020-09-09 14:50:05