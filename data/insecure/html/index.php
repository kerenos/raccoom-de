<html lang="en-US">
    <head>
        <title>Immer diese Waschbären...</title>
        <link rel="stylesheet" href="styles.css">
    </head>
    <body>
        <a href="https://admin.raccoom.de/"><img style="width: 15vw" src="./sticker.png"></a>
        <form action="" method="post">
            <input type="text" name="search">
            <input type="submit" name="submit" value="Search">
        </form>
    <br>
        <?php

            $servername = "db";
            $username = "root";
            $password = getenv("MYSQL_ROOT_PASSWORD");
            $dbname = "raccoom";
    
            $conn  = new mysqli($servername, $username, $password, $dbname);

            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }

            $search_value = $_POST["search"];

            $sql="SELECT * FROM products WHERE prodName LIKE '%$search_value%'";
            $result = $conn->query($sql);

            if ($result->num_rows > 0) {
            echo "
            <table class='maintable'>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Beschreibung</th>
                </tr>";
            while($row = $result->fetch_assoc()) {
                echo "<tr>
                    <td style='width:auto;border:1px solid black;'>".$row["prodId"]."</td>
                    <td style='width:auto;border:1px solid black;'>".$row["prodName"]."</td>
                    <td style='width:auto;border:1px solid black;'>".$row["prodDesc"]."</td>
                </tr>";
            }
            echo "</table>";
            } else {
            echo "0 results";
            }
            $conn->close();
        ?>
    </body>
</html>